/**
 * Setup environment Module.
 * @module src/config/setup
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const { existsSync, mkdirSync, writeFileSync } = require('fs');
const {
  checkIfElasticIndexExists,
  createElasticIndex,
} = require('../lib/elasticClient');

/**
   * function that create the folder and the log file and create
   * the index to logs on elasticsearch.
   * @function
   * @returns {boolean}
   * return boolean informind if the setup was executed with success.
   */
async function setupEnvironment() {
  if (!existsSync('./logs')) {
    mkdirSync('./logs');
  }

  if (!existsSync('./logs/app.log')) {
    writeFileSync('./logs/app.log', '');
  }

  const indexExists = await checkIfElasticIndexExists('mutant');
  if (!indexExists) {
    const indexCreated = await createElasticIndex('mutant');
    if (indexCreated) {
      console.log('index created with success on elastic');
      return indexCreated;
    }
    console.log('error to create index on elastic');
    return indexCreated;
  }
}

module.exports = {
  setupEnvironment,
};
