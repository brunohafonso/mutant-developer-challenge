/**
 * Global configs Module.
 * @module src/config/configOptions
 * @author Bruno Afonso <brunohafonso@gmail.com>
 * @description that module contains the some information used in the project,
 * so we can avoid to rewrite variables in more than one file.
 */
module.exports = {
  elasticUrl: process.env.ELASTIC_URL || 'http://localhost:9200',
  apiUrl: process.env.API_URL || 'https://jsonplaceholder.typicode.com/users',
  elasticUser: process.env.ELASTIC_USER || 'elastic',
  elasticPassword: process.env.ELASTIC_PASSWORD || 'changeme',
};
