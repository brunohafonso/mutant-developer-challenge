/**
 * users route Module.
 * @module src/routes/user
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const {
  getAllData,
} = require('../controllers/user.controller');

/**
   * function that create the routes to users endpoints.
   * @function
   * @param  {object} app - server object.
   */
module.exports = (app) => {
  app
    .get('/', async (req, res) => {
      const { data, statusCode } = await getAllData(req.query.sortby);
      res.status(statusCode).json(data);
    });
};
