/**
 * user controller Module.
 * @module src/controllers/user
 * @author Bruno Afonso <brunohafonso@gmail.com>
 * @description the module contains the functions to return data do endpoints
 * on webservice.
 */
const { getAllUsersData } = require('../services/users.service');
const {
  defaultResponse,
  errorResponse,
} = require('../lib/responseFunctions');


/**
   * function that list all the data of users.
   * @function
   * @param  {string} sortProperty - receive the property name to sort the array.
   * @returns {object}
   * return the object with status code and data so send to endpoint.
   */
async function getAllData(sortProperty) {
  try {
    const usersData = await getAllUsersData(sortProperty);
    return defaultResponse(usersData);
  } catch (error) {
    return errorResponse({ message: `Error to get the data - ${error.message}` });
  }
}

module.exports = {
  getAllData,
};
