/**
 * elastic client Module.
 * @module src/lib/elasticClient
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const http = require('http');

const { elasticUrl, elasticUser, elasticPassword } = require('../config/configOptions');

/**
   * function that return an encoded string in base64 to send to basic auth header on
   * elastic requests.
   * @function
   * @param {string} user - username to auth.
   * @param {string} password - password to auth.
   * @return {object}
   * return an string endoded in base64.
   */
function encodeAuthData(user, password) {
  return Buffer.from(`${user}:${password}`).toString('base64');
}

/**
   * function that return a boolean informing if index exists on elastic.
   * @function
   * @param {string} index - index to check if exists on elastic.
   * @return {boolean}
   * return a boolean informing if index exists on elastic.
   */
function checkIfElasticIndexExists(index) {
  return new Promise((resolve, reject) => {
    const options = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    http.get(`${elasticUrl}/${index}`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        resolve(!!data[index]);
      });
    }).on('error', (err) => {
      if (err) {
        reject(new Error(`Error to get check the elastic index - ${err.message}`));
      }
    });
  });
}

/**
   * function that create a new index on elastic.
   * @function
   * @param {string} index - index to be created on elastic.
   * @return {boolean}
   * return a boolean informing if index was created on elastic.
   */
function createElasticIndex(index) {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    http.get(`${elasticUrl}/${index}`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        resolve(data.acknowledged);
      });
    }).on('error', (err) => {
      if (err) {
        reject(new Error(`Error to get check the elastic index - ${err.message}`));
      }
    });
  });
}

/**
   * function that append a new log data on elastic.
   * @function
   * @param {string} index - index to be created on elastic.
   * @param {string} document - document where will be appended log data on elastic.
   * @param {object} logData - log data that will be appended on elastic.
   * @return {boolean}
   * return a boolean informing if log data was appended with success.
   */
function appendLogToElasticsearch(index, document, logData) {
  return new Promise((resolve, reject) => {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    const req = http.request(`${elasticUrl}/${index}/${document}`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        resolve(!data.error);
      });
    }).on('error', (err) => {
      if (err) {
        reject(new Error(`Error to append log to the elastic index - ${err.message}`));
      }
    });

    req.write(JSON.stringify(logData));
    req.end();
  });
}

module.exports = {
  checkIfElasticIndexExists,
  createElasticIndex,
  encodeAuthData,
  appendLogToElasticsearch,
};
