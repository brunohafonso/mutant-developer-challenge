/**
 * response functions Module.
 * @module src/lib/responseFunctions
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */

/**
   * function that organize the data of response.
   * @function
   * @param {object} response - data to send like response
   * @param {integer} statusCode - the status code of response (default: 200)
   * @returns {object}
   * return an object with response data.
   */
function defaultResponse(response, statusCode = 200) {
  return {
    data: response,
    statusCode,
  };
}

/**
   * function that organize the data of response.
   * @function
   * @param {object} errorResponseMessage - data to send like response
   * @param {integer} statusCode - the status code of response (default: 400)
   * @returns {object}
   * return an object with response data.
   */
function errorResponse(errorResponseMessage, statusCode = 400) {
  return defaultResponse(errorResponseMessage, statusCode);
}

module.exports = {
  errorResponse,
  defaultResponse,
};
