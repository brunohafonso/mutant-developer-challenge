/**
 * response functions Module.
 * @module src/lib/fileFunctions
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const { appendFile } = require('fs');

/**
   * function that organize the data of response.
   * @function
   * @param {object} logData - data to save on logs.
   * @param {string} filePath - path to file to append log data.
   */
function appendLogToFile(logData, filePath) {
  const log = JSON.stringify(logData);
  appendFile(filePath, `${log}\n`, (error) => {
    if (error) {
      throw new Error(`error to save request on log - ${error.message}`);
    }
  });
}

module.exports = {
  appendLogToFile,
};
