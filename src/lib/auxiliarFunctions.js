/**
 * response functions Module.
 * @module src/lib/auxiliarFunctions
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */

/**
   * function format the date to timeZone of são paulo.
   * @function
   * @return {Date}
   * return a date.
   */
function formatDateTime() {
  const options = {
    timeZone: 'America/Sao_Paulo',
    day: 'numeric',
    month: 'numeric',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  };

  const date = new Intl.DateTimeFormat([], options);
  return date.format(new Date());
}

/**
   * function that extract the request details.
   * @function
   * @param {object} req - request object.
   * @param {object} res - response object.
   * @return {object}
   * return an object with request details.
   */
function extractRequestData(req, res) {
  return {
    requestHour: formatDateTime(),
    method: req.method ? req.method : '',
    httpVersion: req.httpVersion ? req.httpVersion : '',
    ip: req.ip ? req.ip : '',
    client: req.headers ? req.headers['user-agent'] : '',
    host: req.headers ? req.headers.host : '',
    path: req.url ? req.url : '',
    statusCode: res.statusCode ? res.statusCode : '',
  };
}

module.exports = {
  formatDateTime,
  extractRequestData,
};
