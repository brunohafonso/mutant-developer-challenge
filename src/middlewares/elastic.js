/**
 * Elastic log Module.
 * @module src/middlewares/elastic
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const { extractRequestData } = require('../lib/auxiliarFunctions');
const { appendLogToElasticsearch } = require('../lib/elasticClient');

/**
   * function works like middleware to get the details of all requests on
   * webservice and append to elastic log.
   * @function
   * @param  {object} req - request object.
   * @param  {object} res - response object.
   * @param  {object} next - next object.
   */
module.exports = (req, res, next) => {
  if (res.headersSent) {
    const logData = extractRequestData(req, res);
    appendLogToElasticsearch('mutant', 'logs', logData);
  } else {
    res.on('finish', () => {
      const logData = extractRequestData(req, res);
      appendLogToElasticsearch('mutant', 'logs', logData);
    });
  }

  next();
};
