/**
 * Log file Module.
 * @module src/middlewares/logFile
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const { extractRequestData } = require('../lib/auxiliarFunctions');
const { appendLogToFile } = require('../lib/fileFunctions');

/**
   * function works like middleware to get the details of all requests on
   * webservice and append to log file.
   * @function
   * @param  {object} req - request object.
   * @param  {object} res - response object.
   * @param  {object} next - next object.
   */
module.exports = (req, res, next) => {
  if (res.headersSent) {
    const logData = extractRequestData(req, res);
    appendLogToFile(logData, './logs/app.log');
  } else {
    res.on('finish', () => {
      const logData = extractRequestData(req, res);
      appendLogToFile(logData, './logs/app.log');
    });
  }

  next();
};
