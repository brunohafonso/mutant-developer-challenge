const express = require('express');

const logFile = require('./middlewares/logFile');
const logElastic = require('./middlewares/elastic');
const usersRoute = require('./routes/users.route');
const { setupEnvironment } = require('./config/setup');

const app = express();

// inject configurations and middlewares
app.use(express.json());
app.set('port', process.env.PORT || 8080);
app.set('json spaces', 4);
app.use(logFile);

// chcking the environment and use elastic middleware just if
// is in docker
if (process.env.DOCKER_ENV) {
  (async function () {
    app.use(logElastic);
    await setupEnvironment();
  }());
}

// injecting the routes
usersRoute(app);

module.exports = app;
