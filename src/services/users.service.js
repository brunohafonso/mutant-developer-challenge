/**
 * users services Module.
 * @module src/services/users
 * @author Bruno Afonso <brunohafonso@gmail.com>
 */
const https = require('https');

const { apiUrl } = require('../config/configOptions');

/**
   * function make a request on webservice of users and return the list with data.
   * @function
   * @returns {array}
   * return the list of objects with the usrs data.
   */
function getApiData() {
  return new Promise((resolve, reject) => {
    https.get(apiUrl, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        resolve(JSON.parse(data));
      });
    }).on('error', (err) => {
      if (err) {
        reject(new Error(`Error to get data from api - ${err.message}`));
      }
    });
  });
}

/**
   * function that list all the users with suite on address.
   * @function
   * @param  {array} usersList - receive the list of data extracted of webservice.
   * @returns {array}
   * return the list of objects with the users filtered with just the items
   * that contains suite on address.
   */
function getUserWithSuite(usersList) {
  return usersList.filter((user) => {
    if (user.address.suite) {
      return user.address.suite
        .toString()
        .toLowerCase()
        .includes('suite');
    }

    return false;
  });
}

/**
   * function that list all the users and extract just email, name and company and sort the list
   * about the parameter sortProperty.
   * @function
   * @param  {array} usersList - list of data extracted of webservice.
   * @param  {string} sortProperty - property that will use to sort the array list (default: name).
   * @returns {array}
   * return the list of objects with the users ordered.
   */
function getUserDataOrderedBy(usersList, sortProperty = 'name') {
  return usersList
    .map(user => ({
      name: user.name || '',
      email: user.email || '',
      company: user.company ? user.company.name : '',
    }))
    .sort((current, next) => (current[sortProperty] > next[sortProperty] ? 1 : -1));
}

/**
   * function that list all the user's websites.
   * @function
   * @param  {array} usersList - list of data extracted of webservice.
   * @returns {array}
   * return the list of user's websites.
   */
function getUsersWebsites(usersList) {
  return usersList
    .map(user => (user.website || ''));
}

/**
   * function that make a request on webservice of users and get all the data to return to
   * endpoint.
   * @function
   * @param  {string} sortProperty - property that will use to sort the array list (default: name).
   * @returns {object}
   * return the object with all data to resnpose to endpoint.
   */
async function getAllUsersData(sortProperty) {
  const usersList = await getApiData();
  return {
    usersWebsites: getUsersWebsites(usersList),
    usersOrderedBy: getUserDataOrderedBy(usersList, sortProperty),
    usersWithSuiteInAddress: getUserWithSuite(usersList),
  };
}

module.exports = {
  getApiData,
  getAllUsersData,
};
