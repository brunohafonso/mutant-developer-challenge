FROM node:slim

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN npm i

EXPOSE 8080

CMD npm run start:test
