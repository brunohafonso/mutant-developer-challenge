const { expect } = require('chai');
const http = require('http');

const {
  elasticUrl,
  elasticUser,
  elasticPassword,
} = require('../../src/config/configOptions');
const {
  encodeAuthData,
  checkIfElasticIndexExists,
  appendLogToElasticsearch,
} = require('../../src/lib/elasticClient');

describe('encodeAuthData() - should return a string in base64', () => {
  it('should return the base64 auth data', () => {
    const result = encodeAuthData('elastic', 'changeme');
    expect(result).to.be.equal('ZWxhc3RpYzpjaGFuZ2VtZQ==');
  });
});

describe('checkIfElasticIndexExists() - should return if an index exists on elastic', function () {
  this.beforeEach((done) => {
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    http.get(`${elasticUrl}/mutant`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        done();
      });
    }).on('error', (err) => {
      if (err) {
        done(new Error(`Error to delete index on the elastic - ${err.message}`));
      }
    });
  });

  it('informing an index that does not exist should return false', async () => {
    const result = await checkIfElasticIndexExists('blablal');
    expect(result).to.be.false;
  });

  it('informing an index that exist should return true', async () => {
    const result = await checkIfElasticIndexExists('mutant');
    expect(result).to.be.true;
  });
});

describe('appendLogToElasticsearch() - should return if an index exists on elastic', function () {
  this.beforeEach((done) => {
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    http.get(`${elasticUrl}/mutant`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        done();
      });
    }).on('error', (err) => {
      if (err) {
        done(new Error(`Error to delete index on the elastic - ${err.message}`));
      }
    });
  });

  it('should return "true" if the log is appended to elastic', async () => {
    const result = await appendLogToElasticsearch('mutant', 'logs', { test: 'test' });
    expect(result).to.be.true;
  });
});
