const { expect } = require('chai');
const {
  getAllData,
} = require('../../src/controllers/user.controller');

const expetedUserWithSuite = {
  id: 2,
  name: 'Ervin Howell',
  username: 'Antonette',
  email: 'Shanna@melissa.tv',
  address: {
    street: 'Victor Plains',
    suite: 'Suite 879',
    city: 'Wisokyburgh',
    zipcode: '90566-7771',
    geo: {
      lat: '-43.9509',
      lng: '-34.4618',
    },
  },
  phone: '010-692-6593 x09125',
  website: 'anastasia.net',
  company: {
    name: 'Deckow-Crist',
    catchPhrase: 'Proactive didactic contingency',
    bs: 'synergize scalable supply-chains',
  },
};

describe(`getAllData() - function that return an object with the websites, 
  users ordered by a property and a list of users with suite word on address`, () => {
  it(`function without parameter should return an object with and array of string with 10 items, 
      an object of users with 10 items ordered 
      by the name (default) and a list with 7 users that have the suite word on address`, async () => {
    const userOrderedExpected = {
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      company: 'Keebler LLC',
    };

    const { data, statusCode } = await getAllData();
    expect(statusCode).to.be.eql(200);
    expect(data.usersWebsites).to.have.lengthOf(10);
    expect(data.usersOrderedBy[0]).to.be.deep.equal(userOrderedExpected);
    expect(data.usersWithSuiteInAddress).to.have.lengthOf(7);
    expect(data.usersWithSuiteInAddress[0]).to.be.deep.equal(expetedUserWithSuite);
  });

  it(`function with parameter 'company' parameter should return an object with and array of string with 10 items, 
      an object of users with 10 items ordered 
      by the name and a list with 7 users that have the suite word on address`, async () => {
    const userOrderedExpected = {
      name: 'Nicholas Runolfsdottir V',
      email: 'Sherwood@rosamond.me',
      company: 'Abernathy Group',
    };

    const { data, statusCode } = await getAllData('company');
    expect(statusCode).to.be.eql(200);
    expect(data.usersWebsites).to.have.lengthOf(10);
    expect(data.usersOrderedBy[0]).to.be.deep.equal(userOrderedExpected);
    expect(data.usersWithSuiteInAddress).to.have.lengthOf(7);
    expect(data.usersWithSuiteInAddress[0]).to.be.deep.equal(expetedUserWithSuite);
  });

  it(`function with parameter 'email' parameter should return an object with and array of string with 10 items, 
      an object of users with 10 items ordered 
      by the name and a list with 7 users that have the suite word on address`, async () => {
    const userOrderedExpected = {
      name: 'Glenna Reichert',
      email: 'Chaim_McDermott@dana.io',
      company: 'Yost and Sons',
    };

    const { data, statusCode } = await getAllData('email');
    expect(statusCode).to.be.eql(200);
    expect(data.usersWebsites).to.have.lengthOf(10);
    expect(data.usersOrderedBy[0]).to.be.deep.equal(userOrderedExpected);
    expect(data.usersWithSuiteInAddress).to.have.lengthOf(7);
    expect(data.usersWithSuiteInAddress[0]).to.be.deep.equal(expetedUserWithSuite);
  });
});
