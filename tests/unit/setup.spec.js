const { expect } = require('chai');
const http = require('http');
const { existsSync, unlinkSync, rmdirSync } = require('fs');
const { resolve } = require('path');

const { encodeAuthData } = require('../../src/lib/elasticClient');
const { setupEnvironment } = require('../../src/config/setup');
const { elasticUrl, elasticUser, elasticPassword } = require('../../src/config/configOptions');

const filePath = resolve(__dirname, '..', '..', 'logs/app.log');
const folderPath = resolve(__dirname, '..', '..', 'logs');

describe(`setupEnvironment() - should create the folder and 
file logs and create the index of logs in the elastic`, function () {
  this.beforeEach((done) => {
    if (existsSync(filePath)) {
      unlinkSync(filePath);
      rmdirSync(folderPath);
    }

    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic ${encodeAuthData(elasticUser, elasticPassword)}`,
      },
    };

    http.get(`${elasticUrl}/mutant`, options, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        data = JSON.parse(data);
        done();
      });
    }).on('error', (err) => {
      if (err) {
        done(new Error(`Error to delete index on the elastic - ${err.message}`));
      }
    });
  });

  it(`should exist the file in the path ./logs/app.log 
    and return true to creation of index ons elastic`, async () => {
    const result = await setupEnvironment();
    expect(result).to.be.true;
    expect(existsSync(filePath)).to.be.true;
  });
});
