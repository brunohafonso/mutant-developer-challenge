# Mutant Developer challenge

O objetivo desse desafio é avaliar o conhecimento dos candidatos

Seguem as instruções:

## Instruções

> Sua tarefa é fazer um aplicativo que carregue a saida da URL https://jsonplaceholder.typicode.com/users, que retorna uma lista de usuário em JSON.

> Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:

1. [x] Os websites de todos os usuários
2. [x] O Nome, email e a empresa em que trabalha (em ordem alfabética).
3. [x] Mostrar todos os usuários que no endereço contem a palavra ```suite```
4. [x] Salvar logs de todas interações no elasticsearch
5. [x] EXTRA: Criar test unitário para validar os itens acima.

> Sua configuração deve ser capaz de ser executado como abaixo:

1. [] Implantar uma máquina virtual. Para não gerar custos para você e para nós, use o `vagrant` com VirtualBox para provisionar sua instância.
2. [x] Entregar o aplicativo usando o Docker e garantir que ele "sobreviva" nas reinicializações

Tenha em mente que o avaliador irá verificar seu repositório e analisará:

1. Design de código
2. Melhores práticas

O avaliador executará seu código e deverá poder ver que o aplicativo está implementado e em execução.

Coisas que você precisa saber
------------------------
1. Não existe resolução certa ou errada.
2. Este aplicativo inicia na porta 8080 no host local.
3. Nós apreciamos Makefiles :)
4. Lembre-se, na máquina do avaliador, não tem nodejs, ansible ou make.
5. Para ver o conteúdo, acesse: http: // \ <host \> /

## instruções de uso

no projeto em questão cria um docker file para o projeto node e dockerfile para o elastic, e um docker-compose para orchestrar o inicio de ambos com as variaveis e configurações necessárias, ambos os containers possuem volume para fazer a persistencia de dados.

além disso o projeto possui testes unitários, lint, documentação com jsDoc, code coverage e um middleware de logs local como backup para os logs do elastic, você podem visualizar os detalhes dos reports nas pastas:

- coverage (possui um arquivo html)
- logs (possui os logs de todas as em formato json linha a linha)
- out vocês consegue visualalizar a documetação do jsDoc
- tests/unit/reports (possui um arquivo com o relatório de lint do código, um report em formato junit e um report em html na pasta html reports)

Para executar o projeto basta realizar o clone do mesmo e rodar o comando:

```
docker-compose up -d --build || docker-compose up --build (caso queira vizualizar os logs do build) 
```

um detalhe sobre o docker-compose inclui um heathcheck para impedir que o app em node suba antes do elasticsearch estiver funcional para que a aplicação não quebre.

em relação ao app node, como solicitado o mesmo está rodando na porta 8080 e como bônus adicionei uma query string (sortby) para que você possam informar o parâmetro para ordenação dos resultados, e busquei seguir ao máximo as melhores práticas de desenvolvimento e usar apenas depedências nátivas do node.


